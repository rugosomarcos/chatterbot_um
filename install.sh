python3 -m venv .
source bin/activate
pip install --upgrade pip
pip3 install -r requirements.txt
python3 -m spacy download en
python3 -m spacy download en_core_web_sm
python3 -m spacy download es