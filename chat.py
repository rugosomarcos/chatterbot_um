from chatterbot import ChatBot
from chatterbot.conversation import Statement

chat = ChatBot(                                               # se declara la entidad del chat, nueva instancia del bot
    'chatterbot',                                             # nombre del bot
    storage_adapter='chatterbot.storage.SQLStorageAdapter',   # adaptador para el almacenamiento en SQLite
    #database='./data/database.sqlite',                       # ubicacion del archivo SQLite donde se guardan los datos
    trainer='chatterbot.trainers.ListTrainer',                # adaptador logico para el entrenamiento
    #logic_adapters=[{                                        # clase que devuelve respuesta
    #    "import_path": "chatterbot.logic.BestMatch",
    #    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
    #    "response_selection_method": "chatterbot.response_selection.get_most_frequent_response"
    #}]
)

def get_feedback():
    text = input()
    if 'si' in text.lower():
        return True
    elif 'no' in text.lower():
        return False
    else:
        print('Por favor escriba "Si" or "No"')
        return get_feedback()


print('Escribe algo para empezar: ')

# The following loop will execute each time the user enters input
while True:
    try:
        input_statement = Statement(text=input())
        response = chat.generate_response(
            input_statement
        )

        print('\n Es esto "{}" una respuesta coherente para "{}"? \n'.format(
            response.text,
            input_statement.text
        ))
        if get_feedback() is False:
            print('Por favor escriba la respuesta correcta: ')
            correct_response = Statement(text=input())
            chat.learn_response(correct_response, input_statement)
            print('Respuesta agregada al bot!!!')

    # Press ctrl-c or ctrl-d on the keyboard to exit
    except (KeyboardInterrupt, EOFError, SystemExit):
        break
